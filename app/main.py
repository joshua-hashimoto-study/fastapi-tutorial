import time
from typing import Optional, Dict, Union

from fastapi import FastAPI, Header, Request, Depends, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles

from .enum import ModelName
from .models import Plane, Car
from .error_handler import UnicornException
from .dependency import CommonSecondQueryParamsDependency, verify_token, verify_key

from .routers import items, users, forms, background_task


# the order of this defines the order of the tags in docs
tags_metadata = [
    {
        "name": "user",
        "description": "Operations with users. The **login** logic is also here.",
    },
    {
        "name": "items",
        "description": "Manage items. So _fancy_ they have their own docs.",
        "externalDocs": {
            "description": "Items external docs",
            "url": "https://fastapi.tiangolo.com/",
        },
    },
]


# set metadata for docs
app = FastAPI(
    title='FastAPI Tutorial Project',
    description="This project is created by going through the FastAPI's official docs",
    version='0.0.1',
    openapi_tags=tags_metadata,
    openapi_url="/api/v1/openapi.json",
    docs_url="/docs",
    redoc_url="/redoc",
)


origins = (
    'http://localhost.tiangolo.com',
    'https://localhost.tiangolo.com',
    'http:/localhost',
    'http://localhost:8080',
    'http://localhost:3000',
)


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


app.mount('/static', StaticFiles(directory='static'), name='static')


async def get_token_header(x_token: str = Header(...)):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


# routers
app.include_router(users.router)
app.include_router(
    items.router,
    prefix='/items',
    tags=['items'],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    forms.router,
    tags=['form'],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    background_task.router,
    tags=['background task']
)


@app.get('/', summary="Create an item",
         response_description="The created item", deprecated=True)
async def root():
    """
    Create an item with all the information:

    - **name**: each item must have a name
    - **description**: a long description
    - **price**: required
    - **tax**: if the item doesn't have tax, you can omit this
    - **tags**: a set of unique tag strings for this item
    """
    print('first endpoint')
    return {'message': 'Hello world'}


@app.get('/model/{model_name}')
async def get_model(model_name: ModelName):
    response_context = {
        'model_name': model_name,
    }
    if model_name == ModelName.alexnet:
        response_context['message'] = 'Deep Learning FTW!'
    elif model_name == ModelName.lenet:
        response_context['message'] = 'LeCNN all the images'
    else:
        response_context['message'] = 'Have some residuals'
    return response_context


@app.get("/files/{file_path:path}")
async def read_file(file_path: str):
    return {"file_path": file_path}


fake_items_db = [
    {'item_name': 'Foo'},
    {'item_name': 'Bar'},
    {'item_name': 'Baz'}
]


@app.get("/required/{item_id}/")
async def required_query_param(item_id: str, needy: str):
    # to make query params required, just don't set a default value
    item = {"item_id": item_id, "needy": needy}
    return item


@app.post("/index-weights/", tags=['dict'])
async def create_index_weights(weights: Dict[int, float]):
    return weights


@app.get("/header/", tags=['header'])
async def read_header(
    authorization: Optional[str] = Header(None)
):
    # 変数名がヘッダーの中のキーになっている
    print(authorization)
    return {"authorization": authorization}
# @app.get("/header/")
# async def read_header(
#     strange_header: Optional[str] = Header(None, convert_underscores=False)
# ):
#     return {"strange_header": strange_header}


vehicles = {
    "item1": {"description": "All my friends drive a low rider", "type": "car"},
    "item2": {
        "description": "Music is my aeroplane, it's my aeroplane",
        "type": "plane",
        "size": 5,
    },
}


@app.get('/vehicles/{vehicle_id}/', response_model=Union[Plane, Car], tags=['union type'])
async def read_vehicle(vehicle_id: str):
    return vehicles[vehicle_id]


@app.get("/keyword-weights/", response_model=Dict[str, float], tags=['dict'])
async def read_keyword_weights():
    return {"foo": 2.3, "bar": 3.4}


# custom exception handler
@app.exception_handler(UnicornException)
async def unicorn_exception_handler(request: Request, exc: UnicornException):
    return JSONResponse(
        status_code=418,
        content={
            "message": f"Oops! {exc.name} did something. There goes a rainbow..."},
    )


@app.get("/unicorns/{name}", tags=['custom error'])
async def read_unicorn(name: str):
    if name == "yolo":
        raise UnicornException(name=name)
    return {"unicorn_name": name}


# @app.get('/depends/', tags=['dependency'])
# async def read_dependency(commons: CommonQueryParams = Depends(CommonQueryParams)):
#     context = {**commons.attributes}
#     return context

# equals to

@app.get('/depends/', tags=['dependency'])
async def read_dependency(commons: CommonSecondQueryParamsDependency = Depends()):
    context = {**commons.attributes}
    return context


# @app.get('/depends/{depend_id}/', tags=['dependency'])
# async def read_dependency(depend_id: int, commons: CommonQueryParams = Depends(CommonQueryParams)):
#     context = {
#         'id': depend_id,
#         **commons.attributes
#     }
#     return context

# equals to

@app.get('/depends/{depend_id}/', tags=['dependency'])
async def read_dependency(depend_id: int, commons: CommonSecondQueryParamsDependency = Depends()):
    context = {
        'id': depend_id,
        **commons.attributes
    }
    return context


# add dependency to decorator
@app.get("/depends/decorator/", dependencies=[Depends(verify_token), Depends(verify_key)], tags=['dependency'])
async def read_depends_decorator():
    return [{"item": "Foo"}, {"item": "Bar"}]


# middleware
@app.middleware('http')
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


# for debug
import uvicorn

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8000)
