from typing import Optional

from fastapi import APIRouter, HTTPException, status, Query, Path
from fastapi.encoders import jsonable_encoder

from ..models import Item

router = APIRouter()


items = {
    'foo': {'name': 'Foo', 'price': 50.2},
    'bar': {'name': 'Bar', 'description': 'The bartenders', 'price': 62, 'tax': 20.2},
    'baz': {'name': 'Baz', 'description': None, 'price': 50.2, 'tax': 10.5, 'tags': []},
}


# by setting response_model_exclude_unset=True it will exclude
# the field that has a default value from the response
@router.get('/{item_id}', response_model=Item, response_model_exclude_unset=True)
async def read_item(item_id: str):
    if item_id not in items:
        # you can even have a custom headers
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail='Item not found', headers={"X-Error": "There goes my error"},)
    return items[item_id]


# @router.get('/')
# async def read_items(q: Optional[str] = Query(None, min_length=3, max_length=50)):
#     context = {
#         'items': [
#             {'item_id': 'foo'},
#             {'item_id': 'bar'}
#         ]
#     }
#     if q:
#         context.update({'q': q})
#     return context
@router.get("/")
async def read_items(
    q: Optional[str] = Query(
        None,
        alias="item-query",
        title="Query string",
        description="Query string for the items to search in the database that have a good match",
        min_length=3,
        max_length=50,
        deprecated=True,
    )
):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results


@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_item(item: Item):
    item_dict = item.dict()
    if item.tax:
        tax_percentage = item.tax / 100
        price_with_tax = item.price + (item.price * tax_percentage)
        item_dict.update(
            {'price_with_tax': price_with_tax, 'tax': f'{item.tax}%'})
    return item_dict


@router.get('/{item_id}/')
async def read_item(item_id: str, q: Optional[str] = None, short: bool = False):
    # short will be type converted from string to bool in this case
    context = {'item': item_id}
    if q:
        context.update({'q': q})
    if not short:
        context.update(
            {"description": "This is an amazing item that has a long description"}
        )
    return context


# @router.put('/{item_id}/')
# async def create_item(item_id: int, item: Item, query: Optional[str] = None):
#     context = {'item_id': item_id, **item.dict()}
#     if query:
#         context.update({'query': query})
#     return context
# @router.put("/{item_id}")
# async def update_item(
#     item_id: int = Path(..., title='The ID of the item to get', ge=0, le=1000),
#     q: Optional[str] = None,
#     item: Optional[Item] = None,
# ):
#     context = {'item_id': item_id}
#     if q:
#         context.update({'q': q})
#     if item:
#         context.update({'item': item})
#     return context
# @router.put("/{item_id}/")
# async def update_item(
#     item_id: int, item: Item, user: User, importance: int = Body(...)
# ):
#     results = {"item_id": item_id, "item": item,
#                "user": user, "importance": importance}
#     return results
@router.put("/{item_id}/", response_model=Item, tags=['custom'], responses={403: {"description": "Operation forbidden"}},)
async def update_item(item_id: str, item: Item):
    update_item_encoded = jsonable_encoder(item)
    items[item_id] = update_item_encoded
    return update_item_encoded


# patch is used for partial update (didn't know that...)
@router.patch("/{item_id}", response_model=Item)
async def update_item(item_id: str, item: Item):
    # get item
    stored_item_data = items[item_id]
    # create Item model
    stored_item_model = Item(**stored_item_data)
    # convert to dict
    # set exclude_unset to True so it will only update
    # the value that is set by the user and not use the default value
    update_data = item.dict(exclude_unset=True)
    # update data using dictionary's copy method
    updated_item = stored_item_model.copy(update=update_data)
    # use jsonable_encoder to convert the data to some that can be stored in DB
    items[item_id] = jsonable_encoder(updated_item)
    return updated_item
