from datetime import datetime

from fastapi import APIRouter, status, Form, File, UploadFile

from ..models import Article

router = APIRouter()


# in fastapi, by using ... it will make the field required
@router.post('/login/', status_code=status.HTTP_201_CREATED)
async def login(username: str = Form(...), password: str = Form(...)):
    print(username, password)
    return {'username': username}


@router.post("/files/")
async def create_file(file: bytes = File(...)):
    return {"file_size": len(file)}


# list of files
# @router.post("/files/")
# async def create_files(files: List[bytes] = File(...)):
#     return {"file_sizes": [len(file) for file in files]}


@router.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...)):
    return {"filename": file.filename}


# list of files
# @router.post("/uploadfiles/")
# async def create_upload_files(files: List[UploadFile] = File(...)):
#     return {"filenames": [file.filename for file in files]}


@router.post('/blog/new/', response_model=Article)
async def create_article(
    cover: UploadFile = File(...),
    title: str = Form(...),
    description: str = Form(...),
    content: str = Form(...),
    timestamp: datetime = Form(...),
    updated: datetime = Form(...)
):
    context = {
        'cover': cover.filename,
        'title': title,
        'description': description,
        'content': content,
        'timestamp': timestamp,
        'updated': updated,
    }
    return context
