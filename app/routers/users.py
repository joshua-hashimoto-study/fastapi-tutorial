from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from ..utils import *
from ..models import AuthOut, User
from ..dependency import oauth2_scheme, get_current_active_user

router = APIRouter()


@router.get("/users/{username}/items/{item_id}/", tags=['user'])
async def read_user_item(
    username: str, item_id: int, q: Optional[str] = None, short: bool = False
):
    item = {'item_id': item_id, 'owner_id': username}
    if q:
        item.update({'q': q})
    if not short:
        item.update(
            {'description': 'This is an amazing item that has a long description'}
        )
    return item


# Don't do this in production!
@router.post("/user/", response_model=AuthOut, tags=['user'])
async def create_user(user: AuthIn):
    user_saved = fake_save_user(user)
    return user_saved


@router.get("/users/me", tags=['user'])
async def read_user_me():
    return {"user_id": "the current user"}


@router.get("/users/{user_id}", tags=['user'])
async def read_user(user_id: str):
    return {"user_id": user_id}


fake_users_db = {
    "johndoe": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "hashed_password": "fakehashedsecret",
        "disabled": False,
    },
    "alice": {
        "username": "alice",
        "full_name": "Alice Wonderson",
        "email": "alice@example.com",
        "hashed_password": "fakehashedsecret2",
        "disabled": True,
    },
}


def fake_hash_password(password: str):
    return "fakehashed" + password


@router.post('/token', tags=['authentication'])
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user_dict = fake_users_db.get(form_data.username)
    if not user_dict:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Incorrect username or password")
    user = AuthInDB(**user_dict)
    hashed_password = fake_hash_password(form_data.password)
    if not hashed_password == user.hashed_password:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail='Incorrect username or password')
    return {'access_token': user.username, 'token_type': 'bearer'}


@router.get('/auth/', tags=['authentication'])
async def read_token(token: str = Depends(oauth2_scheme)):
    return {'token': token}


def fake_decode_token(token):
    return User(
        username=token + "fakedecoded", email="john@example.com", full_name="John Doe"
    )


async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = fake_decode_token(token)
    return user


@router.get("/users/me", tags=['authentication'])
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user
