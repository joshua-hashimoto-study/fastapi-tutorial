from typing import Optional

from fastapi import Header, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordBearer

from .models import User

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


async def common_parameters(q: Optional[str] = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


# dependencies can be classes
class CommonQueryParams:

    def __init__(self, q: Optional[str] = None, skip: int = 0, limit: int = 100):
        self.q = q
        self.skip = skip
        self.limit = limit

    @property
    def attributes(self):
        return {'q': self.q, 'skip': self.skip, 'limit': self.limit}


class CommonSecondQueryParamsDependency(CommonQueryParams):

    def __init__(self, q: Optional[str] = None, skip: int = 0, limit: int = 100, search: Optional[str] = None):
        super().__init__(q, skip, limit)
        self.search = search


async def verify_token(x_token: str = Header(...)):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


async def verify_key(x_key: str = Header(...)):
    # because it returns, this dependency can be used
    # in both decorator and path opetation function
    if x_key != "fake-super-secret-key":
        raise HTTPException(status_code=400, detail="X-Key header invalid")
    return x_key


def fake_decode_token(token):
    return User(
        username=token + "fakedecoded", email="john@example.com", full_name="John Doe"
    )


async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = fake_decode_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user
