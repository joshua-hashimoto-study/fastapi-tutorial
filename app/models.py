from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel, EmailStr


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: float = 10.5
    tags: List[str] = ['all', ]

    class Config:
        schema_extra = {
            'example': {
                'name': 'Foo',
                'description': 'A very nice Item',
                'price': 35.4,
                'tax': 3.2,
                'tags': [
                    'tag1',
                    'tag2',
                ]
            }
        }


# class User(BaseModel):
#     username: str
#     full_name: Optional[str] = None
class User(BaseModel):
    username: str
    email: Optional[str] = None
    full_name: Optional[str] = None
    disabled: Optional[bool] = None


class AuthIn(User):
    password: str


class AuthOut(User):
    pass


class AuthInDB(User):
    hashed_password: str


class BaseVehicle(BaseModel):
    description: str
    type: str


class Car(BaseVehicle):
    type = "car"


class Plane(BaseVehicle):
    type = "plane"
    size: int


class Article(BaseModel):
    cover: str
    title: str
    description: str
    content: str
    timestamp: datetime
    updated: datetime

    class Config:
        schema_extra = {
            'example': {
                'cover': 'image filename',
                'title': 'article title',
                'description': 'article description',
                'content': 'article content',
                'timestamp': 'timestamp',
                'updated': 'updated',
            }
        }
